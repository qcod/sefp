# sefp
我起名太难了还不想太长 ( -'`-; ) (ノへ`、) ╥﹏╥... 
s=server
e=event
f=firewall
p=python

#### 介绍
<pre>
1. logParse.py iis日志分析，按事件频次，ip输出。风险一目了然。
2. servLog.py 服务器事件分析添加防火墙，并日志
3. servWX.py 就是分析上一步的日志然后提醒，支持提醒微信到群
4. toolIP.py 就是上面代码衍生出的一个ip批量查地址功能
</pre>

话不多说直接看源文件

工作中的灵感所写，抛砖引玉
关于服务器安全，欢迎合作。
#### 使用说明
##### logParse.py此文件为日志分析+监控，

```使用说明：
批量或单个分析：python logparse.py a.log b.log
监控iis日志非200事件，超过阈值自动处理。
日志单个监控：python logparse.py自动监控当前目录最新文件，列表提示。
日志批量监控：python logparse.py a.log|b.log
**？子网站比较多，是不是可以多目录多文件，同时监控？正在写**
附上编译过的exe
```
iis日志分析。单日志文件18M只0.5秒分析完成
![输入图片说明](img/logparse.png)
##### servLog.py
**最新版支持一键部署。服务器监控信息上报，
微信预警独立为服务器**
<pre>
服务器收到密码爆破，自动添加ip到防火墙
服务器事件查看18456，然后右键添加计划任务自动执行脚本。就是最终的exe
python脚本思路如下
事件获取
wevtutil qe Application /q:"Event/System/EventID=18456" /c:1 /f:text /rd:true > log.txt
识别log.txt里面的IP然后执行下面cmd添加到防火墙
netsh advfirewall firewall add rule name="FireIP" dir=in action=block remoteip=10.10.10.24
最后打包成exe文件，放到服务器。关联第一步
pyinstaller -F -i icon.ico mane.py 

全文件在serv.py
</pre>
##### servWX.py不想说太多，源文件有说明。我也是新手

22/5/10正常更新，没什么 改动也不想更新了。
1.  是为了单文件匹配多事件，做成命令行模式
2.  有bug当然要改，不管是多大的问题
3.  匹配多事件了，多服务器怎么部署也Ok.就是提醒还有问题。如果是邮件提醒OK.微信提醒一种是微信聊天提醒，还有一种是公众号提醒。
聊天机器人有个帐号登录的问题，要登录一个帐号集中提醒。这方面不想花力气去整。思路可以放一下，就是做一台提醒服务器，然后各终端把日志上传到这里
。可以用上数据库有很后续可能。
在这里多聊一句。公众号提醒很多年前就有一些免费功能。监视服务器状态（网络的状态）并且有多种方式。比如ping服务器ip，或是访问服务器某一web页面，
或是其它自定方式某一端口。访问不可达。就主动发微信提醒。记得多年前用过dnspod直接从dns上监控可以发微信或是邮件一大堆。如果还有其它想聊的，需要
#### 参与贡献

1.  思路来源工作
2.  技术来自网络
3.  欢迎参加

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
