# encoding: UTF-8
'''提醒到微信，已经上线，1分种读一下日志文件有没有更新。
原理是判断上次的日志文件和当前时间 ，如果小于设置的分种，就认为有新攻击就发提醒
然后读日期最新的行，把ip连网查到地址[这个做成了另一个工具，分析txt中的IP]
然后提醒，加了微信程序很简单。可以群一个群，然后有攻击就提醒到群
'''
from threading import Timer
import itchat
from itchat.content import *
import os
import time
import requests
import json
import configparser
cParser=configparser.ConfigParser()
cParser.read("config.ini",encoding="gbk")

ddf = 10 #秒
log_file = 'ip_log.txt'
gname=cParser.get("config","groupName")


userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36"
header = {
    "origin": "//www.baidu.com/baidubot",
    'User-Agent': userAgent,
}

def get_IP(ip):
    url = "https://www.ip138.com/iplookup.asp?action=2&ip="+ip    
    res = requests.get(url, headers=header)
    res.encoding="gbk"
    if res.status_code == 200:
        html = res.text
        st = 'ip_result = {'
        se = ', "ip_c_list"'

        text = html.split(st)[1]
        text = text.split(se)[0]        
        text = '{'+text+'}'
        #text = '{{}}'.format(text)
       addr = json.loads(text).get('ASN归属地').strip()
        print('getIP')
        return addr
    else:
        return ""

#要查日期返回最新行
def readLog():    
    f = open(log_file, 'r',encoding="utf_8_sig")  
    line = '*'
    t = '2000-03-30 12:27:05'   
    for fred in f.readlines():
        tt = fred.split("\t")[0]
        #格式化时间元组再转为时间戳 
        if time.mktime(time.strptime(tt,"%Y-%m-%d %H:%M:%S"))>time.mktime(time.strptime(t,"%Y-%m-%d %H:%M:%S")):
            t=tt
            line=fred        
    f.close()    
    return line.strip()

def writeLog(ip,add):
    with open(log_file,"r+",encoding="utf_8_sig") as f:
        txt = f.read().replace(ip,ip+"#"+add)
        f.seek(0)
        f.write(txt)


def myAuto():    
    #定时器 线程延迟5秒后执行。timer = 
    Timer(ddf, myAuto).start()
    #判断文件时间 
    ticks = os.path.getmtime(log_file)#获取文件的创建时间，修改时间和访问时间
    if (int(time.time()-ticks)>ddf):return
    else:
        #判断文件内容
        lineArr =  readLog().split("\t")
        #print(lineArr)
        ltime = time.mktime(time.strptime(lineArr[0],"%Y-%m-%d %H:%M:%S"))
        if (int(time.time()-ltime)>ddf):return        
        
        ipp = lineArr[1]
        if '#' not in ipp:#判断有没有地址
            ip = ipp
            add = get_IP(ip)
            writeLog(ip,add)
            ipp = "%s#%s"%(ip,add)


        msg = "受到攻击：\n攻击时间：%s\n攻击信息：%s\n攻击次数：%s"%(lineArr[0],ipp,lineArr[2])        
        #print(msg)
        isend(msg)
        
print("正在登录微信……（关窗口退出程序）")
myAuto()


    
def isend(msg):#toRoom为空发给本人    print("!!"+toRoom)
    itchat.send(msg, toRoom)
    time.sleep(1)
    

#退出登录
def logout():
    isend("退出微信")
    return
def ldong():
    isend(localtime+"微信服务就绪")
    print("微信提醒中……")
    return
toRoom=""
localtime =time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

#注册登入登出的回调方法
itchat.auto_login(hotReload=True,loginCallback=ldong, exitCallback=logout)
#itchat.send(localtime+"登录服务器提醒", toUserName='filehelper')
roomslist = itchat.get_chatrooms(update=True)
for roomItem in roomslist:
    #print(roomItem['NickName'])
    if roomItem['NickName'] == gname:
    # if roomItem['NickName'] =="测试专用群":
        toRoom = roomItem['UserName']
        print("提醒群："+gname)
itchat.run(True)