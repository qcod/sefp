# -*- coding: gbk -*-
import os,shutil,sys
import configparser,re,time
import threading,requests,json
import win32com.client
config=configparser.ConfigParser()
cFile="config.ini"
'''
#### 使用说明
24/4/11
还是把终极版发出来，看到的人毕竟不多
源码有很大价值，添加防火墙代码也改进成网段
直接运行，添加计划任务事件。自动关连自身

22/4/30改动有点多，适用多个事件
1.弃用xml.dom 从文本获取IP，这样可以可以适用多事件id因为不同事件的xml格式不一样。
2.增加命令行功能，一参是id 2参数是Security事件 


2022.3.30增加配置文件，选择攻击几次添加防火墙功能
服务器收到密码爆破，自动添加ip到防火墙
服务器事件查看，然后右键添加计划任务自动执行脚本。就是最终的exe

python脚本思路如下
事件获取
wevtutil qe Application /q:"Event/System/EventID=18456" /c:1 /f:text /rd:true > log.txt
识别log.txt里面的IP然后执行下面cmd添加到防火墙
netsh advfirewall firewall add rule name="FireIP" dir=in action=block remoteip=10.10.10.24
最后打包成exe文件，放到服务器。关联第一步
pyinstaller -F -i icon.ico mane.py 
'''

# 检查文件不存在，创建它 
if not os.path.isfile(cFile):
    config.add_section('config')  
    config.set("config","count", "3")
    config.set("config","evid", "18456,17832,17836")
    config.set("config","serverName", "服务器")
    config.set("config","key", "uii099-oiikk-98878")
    config.set("config","sip", "ip:8000")
    config.set("config","level", "0")
    # 写入到文件  
    with open(cFile, 'w', encoding='utf-8') as configfile:  
        config.write(configfile)


config.read(cFile,encoding="utf-8")
_count = config.get("config","count")#3次封禁

def put_response(id,ip,msg):
    sip = config.get("config","sip")
    if (sip):
        api_url="http://"+sip
        data={"k":config.get("config","key"),"id":id,"ip":ip,"m":msg,"s":config.get("config","serverName")}
        r = requests.get(api_url,params =data).json()
        #post(url,data=)
        print(r)#.get("text")
    return 

# 创建id事件触发的任务计划
def create_task_by_event_id(task_name, event_id, program_to_run,par=""):  
    # 创建连接到任务计划程序的COM对象
    scheduler = win32com.client.Dispatch('Schedule.Service')
    scheduler.Connect()
    task_name+= str(event_id)
    # 设置任务基本信息
    task_definition = scheduler.NewTask(0)
    task_definition.RegistrationInfo.Description = task_name+' Event Task'
    task_definition.RegistrationInfo.Author = "qcom.taobao.com" 
    task_definition.Settings.ExecutionTimeLimit = "PT0S"  # 无执行时间限制 
    task_definition.Settings.DisallowStartIfOnBatteries = False
    task_definition.Settings.StopIfGoingOnBatteries = False
    task_definition.Settings.MultipleInstances = 3
    #0并行 1 排队 2勿启动新实例 3停现有实例
    
    task_definition.Principal.UserID = "SYSTEM"
    task_definition.Principal.DisplayName = "SYSTEM"
    task_definition.Principal.LogonType = 5
    task_definition.Principal.RunLevel = 1

    # 创建触发器 - 这里是一个事件触发器
    trigger = task_definition.Triggers.Create(0) 
    trigger.Subscription = f"<QueryList><Query Id='0' Path='Application'><Select Path='Application'>*[System[(EventID={event_id})]]</Select></Query></QueryList>"


    # 创建任务操作 - 这里是运行Python脚本
    action = task_definition.Actions.Create(0)  # 0 表示执行操作
    action.Path = os.path.basename(program_to_run)
    action.Arguments = par 
    action.WorkingDirectory = os.path.dirname(program_to_run)

    # 获取根文件夹
    root_folder = scheduler.GetFolder('\\')

    # 注册任务
    # 参数依次是：任务名称、任务定义、创建或更新任务的标志、用户名、用户域、密码、任务日志记录模式
    task = root_folder.RegisterTaskDefinition(
        task_name,
        task_definition,
        6,  
        None, 
        None,  # 密码 (None 表示使用当前用户)
        3,  # 3 表示使用最高权限运行
        ''  # 不指定 SDDL 字符串
    ) 
    print(f"Task '{task_name}' created successfully.")  

# 获取事件,返回ip或""
def get_event_ip(eid = "18456"):
    log_file = '_log1.txt'
    cmd = 'wevtutil qe Application /q:"Event/System/EventID='+eid+'" /c:1  /rd:true /f:xml'
    os.system('cmd /C "'+cmd+'>'+log_file+'"')#运行不用生成 文件 K保留，C终止，Q关回显
    with open(log_file, 'r', encoding='gbk') as file:  
        fred = file.readline()#.read()
    os.remove(log_file)
    ipt = fred
    if ipt!="":
        #用文本的方式，可以多事件
        ipt = ipt.split('[')[1]
        ipt = ipt.split(']')[0]
        #print(ipt)
        #获取日志当中IP过滤掉除了中文以外的字符
        ipt = re.sub("([^0-9\.])", "", ipt)
        return ipt
    else:
        return ""

# 添加防火墙
def block_ip(ipt):
    level = config.get("config","level")
    ipg = ipt 
    if level=="1":
        ipg = ipt+"/24"
    if level=="2":
        ipg = ipt+"/16"
    if level=="3":
        ipg = ipt+"/8"
    cmd = 'netsh advfirewall firewall add rule name=a'+ipt+' dir=in action=block remoteip='+ipg+''
    #print(cmd)
    os.system('cmd /c "'+cmd)

# 写日志并查ip是否加防火墙
def put_log(b_fp,ipt):        
    if ipt == "" or ipt==0 :return
    localtime =time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    count = 1
    # 检查文件不存在，创建它 
    if not os.path.isfile(b_fp):
        with open(b_fp, 'w', encoding='utf-8') as file:  
            pass  # 不执行任何操作，只是创建文件  
    with open(b_fp,"r",encoding="utf-8") as f:
        lines = f.readlines() 
    with open(b_fp+"b","w",encoding="utf-8") as f_w:
        for text_line in lines:
            if ipt in text_line:
                lineArr = text_line.split("\t")
                #print(lineArr)
                count = int(lineArr[2].strip())+1
                f_w.write("%s\t%s\t%d\t%s\n" % (localtime, lineArr[1],count,eid))
                #continue
            else:
                f_w.write(text_line)
        if(count==1):#如果是新IP            
            f_w.write("%s\t%s\t%d\t%s\n" % (localtime, ipt,1,eid))
    shutil.move(b_fp+"b",b_fp)
    #上面攻击次数是会自动加的 
    msg = "已攻击"+str(count)+"次"
    if(int(count)==int(_count)):#如果>大数会重复添加
        block_ip(ipt)
        msg = "达到"+_count+"次加入防火墙"
    print(ipt,msg)
    _thread = threading.Thread(target=put_response, args=(eid,ipt,msg))
    _thread.start()
    # 注意：这里我们并没有调用 join() 方法，所以主线程不会等待这些线程完成 
    
    return
if __name__ == '__main__':
    if (len(sys.argv)==2):
        eid = sys.argv[1]
        ip = get_event_ip(eid)
        if(ip):
            print(ip)
            put_log('ip_log.txt',ip)

    else:
        par = ""
        exe = sys.executable
        if 'svrLog' not in exe:#py文件
            par = f"{os.path.abspath(__file__)} "
            
        print(os.path.basename(exe))
        # print(str(__file__).split(".")[0])
        print(exe,par)
        evid = config.get("config","evid").split(",")

        for eid in evid:
            create_task_by_event_id("Task by event ", eid,exe,par+str(eid))
        sys.exit("请联系qcom.taobao.com获取使用方法")
