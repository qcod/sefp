#!/usr/bin/env python3
r''' 
iis日志多长时间写入一次？？实测60秒

# 使用说明：此文件为日志分析+监控，
批量分析python logparse.py a.log b.log
解读ip访问所有事件400，200返回最高频次。可以一次分析多个文件。

日志监控：监控iis日志非200事件，超过阈值自动处理。
python logparse.py自动监控当前目录最新文件，列表提示。
python logparse.py a.log|b.log多文件同时监控
以上命令可以用bat自动生成,请参考  echo %time%

readme.md文件里有性能说明，也可以实测
此文件已经完善不再升级。高级功能做其它版本文件
可以对接微信提醒服务 qcom.taobao.com
'''
import time,sys,os
 
class LogMonitor:
    def __init__(self):
        self.__parseTop = 10#日志分析，只显示前10条ip
        self.__threshold = 3#日志监控，出错报警阈值
        self.__blackIp = {}#{ip:3}
 
    def parse_log(self, file_path):
        print(f"日志{file_path}分析")
        _date =None
        _server =None
        dict_id = {}#{'500': 66919, '404': 10289, '200': 6549, '405': 6, '501': 3, '301'
        dict_id_ip = {}
        # 每个事件下，统计IP计数
        with open(file_path,'r', encoding='utf-8', errors='ignore') as log_file:
            data = log_file.readlines()
            for line in data: 
                # 空行不处理 line[0:1]=="#"
                if line == '\n' or line.startswith("#"):continue
                if not _date: _date = line[0:10]
                list_line = line.split()
                if len(list_line)>10:
                    if not _server :_server = f"{list_line[2]}:{list_line[6]}"
                    id = list_line[10]
                    ip = list_line[8]
                    if id in dict_id:#如何判断有没有key
                        dict_id[id] +=1
                    else:
                        dict_id[id] =1
                        dict_id_ip[id]={}
                    dict_id_ip[id][ip] = dict_id_ip[id][ip]+1 if ip in dict_id_ip[id] else 1
                    # list.append(list)
            log_size = log_file.tell()
        # 以上分析数据完成，
        # 逆序排序后的字典  , reverse=True
        sorted_dict_id = dict(sorted(dict_id.items(), key=lambda item: item[1], reverse=True))
        # print(sorted_dict_id)
        
        print(f"日志大小：{self.get_size(log_size)}")
        print(f"发生日期：{_date}")
        print(f"发生服务：{_server}")
        # 以下输出生成输出
        # 使用sum()函数和生成器表达式将字典的值加起来  
        total = sum(value for value in dict_id.values()) 
        print(f"事件条数：{total}\t\t(只显示访问量前几IP)")  # 输出：10
        print("_"*50)
        
        
        # for key in sorted_dict_id: # 遍历字典的键  
        # for value in sorted_dict_id.values():遍历字典的值  
        for key, value in sorted_dict_id.items(): # 同时遍历字典的键和值   
            print(f"事件：{key:20s}{value}条")
            temp_dict = dict(sorted(dict_id_ip[key].items(), key=lambda item: item[1], reverse=True))
            flag = 0
            for k, v in temp_dict.items():
                flag+=1
                if flag > self.__parseTop:break
                print(f"\tip：{k:20s}{v}次")
                
                
        print(f"列表长度：{len(data)}")
        # print(f"列表内存：{self.get_size(sys.getsizeof(data))}")
        print(" "*10+"[END]\n"+"_"*50)
    
    def get_size(self,size_bytes):
        if size_bytes>(1024 * 1024):
            size = f"{size_bytes / (1024 * 1024):.3f} MB"
        elif size_bytes > 1000:
            size = f"{(size_bytes / 1024):.2f} KB"
        else:
            size = f"{size_bytes} byte"
        return size
        
    # 监控文件入口。   文件名用watch来做。
    def monitor(self, file_path, callback):
        self.callback = callback
        #最后位置,如果file——path是列表，这个也要跟上
        _file_list = file_path.split('*')
        _file_position = [0]*len(_file_list)
        # print(_file_position)#记录读取位置
        # _file_position = [0 for _ in range(len(_file_list))] 列表推导
        print(f"开始监控...\n",_file_list)
        try:
            while True:
                # list没有key要这样遍历_file_list.items()for index, value in enumerate(my_list):  
                for file in _file_list:
                    key = _file_list.index(file) 
                    _file_position[key] = self.monitor_file(file,_file_position[key])
                time.sleep(0.8)# 等待一段时间新开始监控
        except KeyboardInterrupt:
            print(" END")
            
    # 监控文件逻辑
    def monitor_file(self,file_path,last_position):
        if not os.path.isfile(file_path):return
        with open(file_path, 'r') as log_file:
            #移到结尾
            if last_position==0:log_file.seek(0,2)
            # 删除行了
            # if  last_position >log_file.tell():
            else:
                log_file.seek(last_position)#超过文件长度
                # last_position = log_file.tell()不兼容删行，没有到EOF
                for line in log_file.readlines():
                    self.check_line(line)
                log_file.seek(0,2)#兼容删行
            last_position = log_file.tell()
        return last_position
            
    # 监控文件，匹配规则并显示,不等于200
    def check_line(self,line):
        #有空行不处理
        if line.strip()=="" or line.startswith("#"):return
        list_line = line.split()
        if len(list_line)>10:
            id = list_line[10]
            ip = list_line[8]
            # print(ip,id)#test
            if id != "200" :#监测非法事件和IP,类同分析
                # 攻击输出，
                _localtime =time.strftime("%H:%M:%S", time.localtime())
                _server = f"{list_line[2]}:{list_line[6]}{list_line[4]}"
                #判断攻击次数，不按服务和事件，只按IP
                self.__blackIp[ip] = self.__blackIp[ip]+1 if ip in self.__blackIp else 1
                # print(ip,self.__blackIp)
                print(f"{_localtime} 事件:{id}\t{ip}攻击{self.__blackIp[ip]}次\t//{_server}")
                # 暂时不要加防火墙，观察一段时间self.blackip(ip)
            
    #报警规则,并处理
    def blackip(self,ip):
        _count=self.__blackIp[ip]
        if _count >= self.__threshold:
            msg = f'{ip}攻击{_count}次，达到安全阈值'
            # 完整脚本名，去掉路径，再去掉后缀
            script_name = os.path.splitext(os.path.basename(__file__))[0]
            print(msg) # 记录日志
            with open(script_name+".txt", 'a+') as file:
                file.write(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())+"\t," +msg+"\n" )
            self.callback(ip)
            
# 回调函数 添加防火墙
def block_ip(ipt):
    level = 1
    ipg = ipt 
    if level=="1":
        ipg = ipt+"/24"
    if level=="2":
        ipg = ipt+"/16"
    if level=="3":
        ipg = ipt+"/8"
    cmd = 'netsh advfirewall firewall add rule name=b'+ipt+' dir=in action=block remoteip='+ipg+''
    os.system('cmd /c "'+cmd)
 
# 示例使用
if __name__ == "__main__":
    start=time.time()
    lm = LogMonitor()
    
    # 手动解析文件
    if len(sys.argv) >1:
        if "*" in sys.argv[1]:
            #监控多个文件，不要用｜
            # print(sys.argv[1])
            lm.monitor(sys.argv[1],  block_ip)
        else:#分析日志
            for a in sys.argv[1:]:
                lm.parse_log(a)
    else:
        # 自动获取当前目录最新文件
        directory = '.'
        if os.path.isdir(directory):
            # 初始化最新的文件及其修改时间  
            latest_file = None  
            latest_mtime = 0 
            # 遍历目录中的文件和子目录  root=.      dirs子目录
            for root, dirs, files in os.walk(directory):  
                for file in files:
                    if not ".log" in file :continue
                    # 构建文件的完整路径  ./file
                    # file_path = os.path.join(root, file)  
                    file_path =  file
                    # 获取文件的修改时间  
                    # 这里有个问题，日志是打开就一直写保存，不关闭所以修改时间不变。
                    # 所以要用建立时间。----因为不同的文件系统对创建时间的处理是不同的。要用条三方库
                    mtime = os.path.getmtime(file_path)  
                    # 如果找到比当前最新的文件还要新的文件，则更新latest_file和latest_mtime  
                    if mtime > latest_mtime:  
                        latest_file = file_path  
                        latest_mtime = mtime  
              
            # 监控文件print(latest_file)
            lm.monitor(latest_file,  block_ip)
    
    
    end=time.time()
    print(f'运行时间: %s Seconds'%(round(end-start,4)))
