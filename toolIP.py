# encoding: UTF-8
'''
延伸小工具，查IP地址
可以用多线程
'''

from threading import Timer
import re
import os
import time
import requests
import json


log_file = 'toolIP.txt'
userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36"
header = {
    "origin": "//www.baidu.com/baidubot",
    'User-Agent': userAgent,
}

def get_IP(ip):
    url = "https://www.ip138.com/iplookup.asp?action=2&ip="+ip    
    res = requests.get(url, headers=header)
    res.encoding="gbk"
    if res.status_code == 200:
        html = res.text
        st = 'ip_result = {'
        se = ', "ip_c_list"'

        text = html.split(st)[1]
        text = text.split(se)[0]        
        text = '{'+text+'}'
        addr = json.loads(text).get('ASN归属地').strip()
        return addr
    else:
        return ""

    

def writeLog():
    with open(log_file,"r+",encoding="utf-8") as f:
        txt = f.read().strip()
        for ip in txt.split("\n"):
            if re.match(r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", ip):
                add = get_IP(ip)
                print(ip+"\t"+add)
                txt = txt.replace(ip,ip+"\t"+add)
        f.seek(0)
        f.write(txt)       
    
print("开始运行...")

writeLog()

print("结束运行...")
